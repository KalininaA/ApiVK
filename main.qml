import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Window 2.3

/*
  http://doc.qt.io/qt-5/qtqml-cppintegration-topic.html
  http://doc.qt.io/qt-5/qtqml-cppintegration-overview.html

  1)Привязка С++ слотов к QML сигналам
  2)Получение ссылок на QML объекты в С++ коде (делает видимым QML объеты в С++)
  3)Помещение С++ функций и объектов в область видимости QML
  4)Привязка QML слотов к сигналам С++ объектов
  */

ApplicationWindow {
    signal sendAuth(string login, string password)
    visible: true
    width: 240
    height: 280
    title: qsTr("Tabs")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page{
            title: "Лр1: Безопасная аутентификация"
            ColumnLayout{
                anchors.fill: parent; //привязка к границам окна

                Item{//пустой заполнитель
                    Layout.fillHeight: true
                }



                TextField{
                    id: editLogin
                    Layout.alignment: Qt.AlignHCenter //центровка
                    placeholderText: "Login"
                    horizontalAlignment: TextInput.AlignHCenter
                    background:  Rectangle{
                        id: loginHightLight
                        anchors.fill: parent
                        implicitWidth: Screen.pixelDensity * 40
                        color: "transparent"
                        border.color: "transparent"

                        ScaleAnimator{
                            id: animation
                            target: loginHightLight //цель анимации
                            easing.type: Easing.OutElastic //тип кривой анимации
                            from: 0.5 // от
                            to: 1 // до
                            duration: 500 //длительность
                            running: false //по умолчанию анимация остановлена
                        }
                    }
                }

                TextField{
                    id: editPassword
                    Layout.alignment: Qt.AlignHCenter
                    placeholderText: "Passwrd"
                    horizontalAlignment: TextInput.AlignHCenter
                    passwordMaskDelay: 800
                    echoMode: TextInput.Password //TextInput.PasswordEchoOnEdit
                    background:  Rectangle{
                        id: passwordHightLight
                        anchors.fill: parent
                        implicitWidth: 200
                        color: "transparent"
                        border.color: "transparent"

                        ScaleAnimator{
                            id: animation2
                            target: loginHightLight //цель анимации
                            easing.type: Easing.OutElastic //тип кривой анимации
                            from: 0.5 // от
                            to: 1 // до
                            duration: 500 //длительность
                            running: false //по умолчанию анимация остановлена
                        }
                    }
                }



                Button{
                    id: btnAuth
                    //x: 300;
                    // y: 90;
                    text: "Вход"
                    Layout.alignment: Qt.AlignHCenter
                    Layout.minimumHeight: Screen.pixelDensity * 15
                    Layout.minimumWidth: Screen.pixelDensity * 15
                    onClicked: {
                        console.log("Log = " + editLogin.text + ", Pass = " + editPassword.text)
                        if(editLogin.text == ""){
                            //изменение цвета прямоугольника на красный
                            loginHightLight.border.color = "red";
                            animation.start()
                            return
                        }
                        if(editPassword.text == ""){
                            //подсветка красным
                            passwordHightLight.border.color = "red";
                            animation2.start()
                            return
                        }
                        sendAuth(editLogin.text,editPassword.text)
                    }


                }
                Item{//пустой заполнитель
                    Layout.fillHeight: true
                }
            }
        }

        Page {
            id: vkFriends
            Rectangle {
                id: backgroundfriends
                anchors.fill: parent
                color: "transparent"
            }
            Item {
                id: item1
                anchors.fill: parent
                   ListView {
                       anchors.fill: parent
                       model: modelFriends
                       spacing: 20
                       delegate: Rectangle {
                               id: rec1
                               color: "white"
                               height: 120
                               width: parent.width
                               radius: 30
                               anchors.margins: 20
                               opacity: 0.7

                               GridLayout {
                                   anchors.fill: parent
                                   opacity: 1
                                   Image {
                                       source: photo
                                       anchors.right: parent.right
                                       anchors.top: parent.top
                                       anchors.rightMargin: 25
                                       anchors.topMargin: 10
                                       Layout.row: 1
                                       Layout.column: 2
                                       Layout.rowSpan: 4
                                   }

                                   Text {
                                       font.pointSize: 12
                                       color: "black"
                                       anchors.left: parent.left
                                       anchors.leftMargin: 15
                                       Layout.row: 1
                                       Layout.column: 1
                                       anchors.horizontalCenter: parent.horizontalCenter
                                       text: "ID друга - " + friendid
                                   }
                                   Text {
                                       font.pointSize: 12
                                       color: "black"
                                       anchors.left: parent.left
                                       anchors.leftMargin: 15
                                       Layout.row: 2
                                       Layout.column: 1
                                       anchors.horizontalCenter: parent.horizontalCenter
                                       text: "Имя друга: " + friendname
                                   }
                               }
                           }
                   }
            }
        }


        Page{
            ListView{
              anchors.fill: parent
              model: 5
              /*delegate: Text{text:index;
                  font.bold:true;
                  font.pixelSize: 20}*/

             /* delegate:Rectangle{
                  width: parent.width
                  height: Screen.pixelDensity*10
                  gradient: Gradient {
                  }
              }*/

              delegate: // C
              ColumnLayout{

              Item{ // пустой заполнитель
              Layout.fillHeight: true
              }

              Rectangle{
              width: parent.width
              height: Screen.pixelDensity * 10

              gradient: Gradient{
              GradientStop{
              position: 0.00;
              color: "#fdd0d0"
              }
              GradientStop{
              position: 0.49;
              color: "#ff0000"
              }
              GradientStop{
              position: 1.00;
              color: "#fdd0d0"
              }
              }
              radius: 5
              border.width: 1
              border.color: "red"
              Text{
              text:index
              font.bold: true
              font.pixelSize: 20
              }
              }

              //model: ["элемент1","Элемент2","Элемент3"]
              //model:exampleModel
        }
    }

    /*
    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Page 1")
        }
        TabButton {
            text: qsTr("Page 2")
        }
    }*/

    Connections{
        target: authController // подсвеычивается синим - значит успешно введен
        // в область видимости QML из С++
       onSignalAuthSuccess:{
           console.log("Вызван обработчик");
            swipeView.currentIndex = 1
       }
    }

}

//        Page {//мультимедиа
//            MediaPlayer {
//            id: mediaplayer
//            autoLoad: true //грузить файл сразу, без команды
//            loops: MediaPlayer.Infinite //число повторений
//        //autoPlay:true


//        /* ТИПЫ URL в QML
//        ссылка, рассчитанная на автоопределение источника (ресурсы или файлы)
//        source: "sample.mp4"
//        source: ":/...."
//        source: ":///...."

//        // явное указание на ресурсы
//        source: "qrc:///...."
//        source: "qrc:/...."

//        // явное указание на файлы
//        source: "file:/...."
//        source: "file:///...."
//        source: "file:...."*/


//        source: (Qt.platform.osi ==="android")?
//                    ("file:///storage/emulated/0/Download/sample.avi"):
//            ("file:///C:/Users/alext/Documents/QtRaptor/sample.avi");
//        onError: {
//           console.debug("mediaplayer video error\n");
//            console.debug(errorString);
//        }

//        }
//        ColumnLayout{
//        anchors.fill: parent

//        VideoOutput {
//        enabled: true
//        Layout.fillWidth: true
//        Layout.fillHeight: true
//        //anchors.fill: parent
//        source: mediaplayer //mediaplayer //либо камера
//        }

//        Button
//        //НА АВТОМАТ:
//        //сделать анимацию для кнопки
//        //регистрацию через какой-либо еще сервер
//        //сделать раньше чем он прогрузку в лист аватарок и имен из вк в лист
//        {
//        //text: "start"
//            id: btnStartStop
//            clip:true
//            Layout.preferredWidth: Screen.pixelDensity * 30
//            Layout.preferredHeight: Layout.preferredWidth/3.5
//            Layout.alignment: Qt.AligHCenter
//            background : Item{
//            clip:true
//            Image{
//            id:imgIcon
//            width : parent.width
//            source: "buttons.png"
//            fillMode: Image.PreserveAspectFit
//            clip: true
//            }}

//        onClicked:
//        if(mediaplayer.playbackState == MediaPlayer.PausedState
//                || mediaplayer.playbackState == MediaPlayer.StoppedState){
//            mediaplayer.play();
//            imgIcon.y=-btnStartStop.height * 3.55;
//            //text="Stop"
//        }
//        else{
//            mediaplayer.stop();
//            //text="Start"
//        }
//        }
//        }
//        }


//        Page{
//            //работа с камерой

//        }


//    }

//    Drawer{
//        width: 2/3*parent.width
//        height: parent.height

//        ListView{
//            id:lstPagesMenu
//            model: swipeView.count
//            anchors.fill:parent
//            anchors.margins: 10
//            delegate: Button{
//            text: index
//            width: lstPagesMenu.width
//            height: Screen.pixelDensity*12
//            onClicked: {
//                swipeView.currentIndex=index
//            }
//            }
//            }

//        }
//}

}}
