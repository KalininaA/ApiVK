#ifndef AUTHCONTROLLER_H
#define AUTHCONTROLLER_H

#include <QObject>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QEventLoop>
#include <QStringList>
#include <QAbstractListModel>
#include <QThread>


class FriendsObject {
    public:
        FriendsObject (const QString &FriendId,
                       const QString &FriendName,
                       const QString &Photo);

        QString FriendId() const;
        QString FriendName() const;
        QString Photo() const;
    private:
        QString friend_id;
        QString friend_name;
        QString friend_photo;
};


class FriendsModel : public QAbstractListModel {
    Q_OBJECT
public:
    enum DataRoles {
        FriendIdRole,
        FriendNameRole,
        PhotoRole,
        StatusRole
    };

    FriendsModel(QObject *parent = 0);

    void addFriend(const FriendsObject & newFriend);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<FriendsObject> friends_mod;

};

class AuthController : public QObject
{
    Q_OBJECT
public:
    explicit AuthController(QObject *parent = nullptr);
    QNetworkAccessManager * na_manager;
    FriendsModel friends_model;
signals:
    signalAuthSuccess();
public slots:
   void Authentificate(QString login, QString password); //параметры такие же, как и в сигнале
};

#endif // AUTHCONTROLLER_H
