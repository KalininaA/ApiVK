#include "tcpcontroller.h"
#include <QTcpServer>
#include <QDebug>

/* класс,инкапсулирующий
* 1)сокет (конечную точку с буфером,связанную с сетевым адаптером)
* 2) бесконечный цикл попыток чтения из сокета (т.н. "прослушка")
* 3)генерация сигналов при поступлении каких-то данных на сокет сервера
*/

#include <QTcpSocket>
/* класс,инкапсулирующий
* 1) буфер данных ,который будет отправляться/считываться с адаптера
* 2) функции для чтения/записи
* 3) остальной функционал типичного устройства ввода/вывода (наподобие QFile, QIODevice и т.д.)
* 4) поддержка нескольких соединений

*/

/* Цикл работы с Tcp-сервером
* 1)создать объект
* 2)создать слот-обработчик данных с сервера
* 3)привязать слот для обработки поступающих данных
* QObject:connect (&имя_сервера, SIGNAL ( finished()),
* &имя_объекта, SLOT(...))
* 4) запустить "прослушку" serv.listen(ip-адрес,порт)
*
*
* Цикл работы с TCP-сокетом
* 1)создать сокет
* 2) Отправить данные socket.write() либо считать socket.read()
*/
tcpcontroller::tcpcontroller(QObject *parent) : QObject(parent)
{
    serv = new QTcpServer();
    client_sock=new QTcpSocket;
    server_sock=new QTcpSocket;

    //QTcpServer serv; // пункт 1 конспекта

    // 3 пункт конспекта: привязка сигнала к слоту
    QObject::connect(serv, SIGNAL(newConnection()),
                     this, SLOT(newConnection()));

    serv->listen (QHostAddress::LocalHost,
                 33333); // 4 пункт конспекта, запуск бесконечного цикла прослушки

    //QTcpSocket client_sock;
    //client_sock.bind(QHostAddress::LocalHost,
                  //   33333);
    //из-за ограниченности условий имитируем входящее подключение вторым сокетом на localhost
    QObject::connect(client_sock, SIGNAL(readyRead()),
                     this, SLOT(clientReady()));
     client_sock->connectToHost(QHostAddress::LocalHost,
                        33333);
}

void tcpcontroller::newConnection()// пункт 2 конспекта
{
    //получение и хранение ссылки на сокет нового входящего соединения
    //который себе уже создал сервер
    qDebug()<< "*** newConnection()";
    server_sock = serv->nextPendingConnection();
    server_sock->write("Hello, world");
}
void tcpcontroller::clientReady() //слот для обработки содержимого клиентского сокета
{
    qDebug()<< "*** Полученные данные"<< client_sock->readAll();
}
