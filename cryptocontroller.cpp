#include "cryptocontroller.h"
#include <QFile>
#include <QIODevice>

#include <openssl/conf.h> // функции, структуры и константы настройки OpenSSL
#include <openssl/conf.h>
#include <openssl/evp.h> // сами криптогрфические функции https://wiki.openssl.org/index.php/EVP
#include <openssl/err.h> // коды внутренних ошибок OpenSSL и их расшифровка
#include <openssl/aes.h>

CryptoController::CryptoController(QObject *parent) : QObject(parent)
{

}

void CryptoController::encryptFile(unsigned char * key){
    //QFile f0(fileName),
    // f_encrypted("f_encrypted.txt");
    //Вписать в объявление функции unsigned char * key вместо всего QString fileName, QString key, QString iv

    unsigned char in_buf[1024] = {0},
            out_buf[1024] = {0};

    unsigned char iv[] = "123456789";

    QString in_file_name, out_file_name;

#ifdef Q_OS_ANDROID
    in_file_name="/storsge/emulated/0/Download/f_encrypted.txt";
    out_file_name="/storsge/emulated/0/Download/f_decrypted.txt";
#else
    out_file_name = "f_encrypted.txt";
    in_file_name = "f0.txt";
#endif

    QFile f_in(in_file_name),
            f_out(out_file_name);
    f_in.open(QIODevice::ReadOnly);
    f_out.open(QIODevice::WriteOnly
               | QIODevice::Truncate);

    EVP_CIPHER_CTX * ctx;
    ctx = EVP_CIPHER_CTX_new();
    EVP_EncryptInit_ex(ctx,
                       EVP_aes_256_cbc(),
                       NULL,
                       key,
                       iv);
    int ln1 = f_in.read((char*)in_buf, 1024);
    int ln2 = 1024, tmpln = 0;

    while (ln1>0)
    {
        if(!EVP_EncryptUpdate(ctx,
                              out_buf, //выходной параметр : ссылка, куда
                              &ln2,
                              in_buf, // входной параметр: что шифровать
                              ln1)) // входной параметр : длина входных данных

        {
            /* Error */
            return;
        }

        f_out.write((char*)out_buf, ln2);
        ln1 = f_in.read((char*)in_buf,1024);
    }


    if (!EVP_EncryptFinal_ex(ctx, out_buf, &tmpln))
    {
        /* Error */
        return;
    }

    f_out.write((char*)out_buf, tmpln);
    EVP_CIPHER_CTX_free(ctx);
    f_out.close();
    f_in.close();
    return;

    /*
f0.open(QIODevice::ReadOnly);
f_encrypted.open(QIODevice::WriteOnly
| QIODevice::Truncate);

char buffer[256] = { 0 };
char out_buf[256] = { 0 };
EVP_CIPHER_CTX * ctx = EVP_CIPHER_CTX_new();

EVP_EncryptInit_ex(ctx, // структура, заполняемая настройками
EVP_aes_256_cbc(), // 1) метод шифрования
// cbc - независимое шифрование порций
// наиболее простая и наименее защищенная схема
// зато любая порция может быть расшифрована независима
NULL, // 2)...

// 3) в виде массива unsigned char заносится пароль
// используется (приведение типов *)
(unsigned char *)key.toStdString().c_str(),

// 4) iv - initialization vector
// заносится аналогичным образом из строки

(unsigned char *)iv.toStdString().c_str());
int len1 = 0;
int len2 = f0.read(buffer, 256);
while (!f0.atEnd()) // цикл, пока позиция read() менее длины файла
{
// шифрование порции
EVP_EncryptUpdate(ctx, // объект с настройками

// выходной буфер с шифрованными данными и его длина
(unsigned char *)out_buf, //выходной параметр : ссылка, куда
&len1, // выходной параметр: длина полученного шифра

// входной буфер с фрагментов файла и его длина
(unsigned char*)buffer, // входной параметр: что шифровать
len2); // входной параметр : длина входных данных

// вывод зашифрованной порции в файл
f_encrypted.write(out_buf, len1);
// считывание следующей порции

// QFile::read() не только считывает данные,
// но и двигает текущую "позицию" в QFile
len2 = f0.read(buffer, 256);
}
EVP_EncryptFinal_ex(ctx, (unsigned char *)out_buf, &len1);
f_encrypted.write(out_buf, len1);

f_encrypted.close();
f0.close();

*/
}

void CryptoController::decryptFile(unsigned char * key){
    //QFile f0(fileName),
    // f_encrypted("f_encrypted.txt");
    //Вписать в объявление функции unsigned char * key вместо всего QString fileName, QString key, QString iv

    unsigned char in_buf[1024] = {0},
            out_buf[1024] = {0};

    unsigned char iv[] = "123456789";

    QString in_file_name, out_file_name;

    in_file_name = "f_encrypted.txt";
    out_file_name = "f_decrypted.txt";

    QFile f_in(in_file_name),
            f_out(out_file_name);
    f_in.open(QIODevice::ReadOnly);
    f_out.open(QIODevice::WriteOnly
               | QIODevice::Truncate);

    EVP_CIPHER_CTX * ctx;
    ctx = EVP_CIPHER_CTX_new();
    EVP_DecryptInit_ex(ctx,
                       EVP_aes_256_cbc(),
                       NULL,
                       key,
                       iv);
    int ln1 = f_in.read((char*)in_buf, 1024);
    int ln2 = 1024, tmpln = 0;

    while (ln1>0)
    {
        if(!EVP_DecryptUpdate(ctx,
                              out_buf, //выходной параметр : ссылка, куда
                              &ln2,
                              in_buf, // входной параметр: что шифровать
                              ln1)) // входной параметр : длина входных данных

        {
            /* Error */
            return;
        }

        f_out.write((char*)out_buf, ln2);
        ln1 = f_in.read((char*)in_buf,1024);
    }


    if (!EVP_DecryptFinal_ex(ctx, out_buf, &tmpln))
    {
        /* Error */
        return;
    }

    f_out.write((char*)out_buf, tmpln);
    EVP_CIPHER_CTX_free(ctx);
    f_out.close();
    f_in.close();
    return;

}

