#include "authcontroller.h"
#include <QDebug>

FriendsObject::FriendsObject(const QString &FriendId, const QString &FriendName, const QString &Photo)
    :   friend_id(FriendId),
        friend_name(FriendName),
        friend_photo(Photo)
{

}


QString FriendsObject::FriendId() const {
    return friend_id;
}

QString FriendsObject::FriendName() const {
    return friend_name;
}

QString FriendsObject::Photo() const {
    return friend_photo;
}


FriendsModel::FriendsModel(QObject *parent)
{

}

void FriendsModel::addFriend(const FriendsObject &newFriend) // функция добавления объекта друга в модель
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount()); // начало записи в модель
    friends_mod << newFriend; // сама запись объекта в модель
    endInsertRows();  // сохранение модели
}


int FriendsModel::rowCount(const QModelIndex &parent) const // функция отображения кол-ва объектов в модели
{
    Q_UNUSED(parent);
    return friends_mod.count(); // возвращает количество элементов

}

QVariant FriendsModel::data(const QModelIndex &index, int role) const // функция возвращения элементов объекта в модели
{
    if (index.row() < 0 || index.row() >= friends_mod.count())
            return QVariant();
    const FriendsObject &itemToReturn = friends_mod[index.row()]; // создается локальный объект друга, которому присваивается объект в модели по индексу строки
    if (role == FriendIdRole) // если обращение к роли FriendId
        return itemToReturn.FriendId(); // возвращается FriendId объекта друга
    else if (role == FriendNameRole) // если обращение к роли FriendName
        return itemToReturn.FriendName(); // возвращается FriendName объекта друга
    else if (role == PhotoRole) // если обращение к роли Photo
        return itemToReturn.Photo(); // возвращается Photo объекта друга

    return QVariant();


}

QHash<int, QByteArray> FriendsModel::roleNames() const // прописываются имена ролей
{
    QHash<int, QByteArray> roles;
    roles[FriendIdRole] = "friendid";
    roles[FriendNameRole] = "friendname";
    roles[PhotoRole] = "photo";
    return roles;

}



AuthController::AuthController(QObject *parent) : QObject(parent)
{
    na_manager = new QNetworkAccessManager();
}

void AuthController::Authentificate(QString login, QString password)
{
    qDebug() << "слот сработал: log = " << login << " pass = " << password;
    emit signalAuthSuccess();

    QString clientID = "6444409";

    //Строчки далле следуют для синхронного ожидания ответа от сервера
        QEventLoop loop;
        connect(na_manager,SIGNAL(finished(QNetworkReply*)),&loop,SLOT(quit()));


    QNetworkReply * reply = na_manager->get(
                    QNetworkRequest(
                        QUrl(
                        "https://oauth.vk.com/authorize"
                        "?client_id=" + clientID +
                        "&display=mobile"
                        "&redirect_uri=https://oauth.vk.com/blank.html"
                        "&scope=friends"
                        "&response_type=token"
                        "&v=5.37"
                        "&state=123456"
                        )
                    )
                );
    //https://oauth.vk.com/authorize?client_id=6444409&display=mobile&redirect_uri=https://oauth.vk.com/blank.html&scope=friends&response_type=token&v=5.37&state=123456

    loop.exec();

//Должна была прийти форма авторизации
    QString str(reply->readAll());
     qDebug() << "ответ от сервера = " << str;

     //Находим параметры из формы авторизации
     int tag_position1 = str.indexOf("ip_h");
     int tag_position2 = str.indexOf("_origin");
     int tag_position3 = str.indexOf("lg_h");
     int tag_position4 = str.indexOf("\"to");
            QString ip_h = str.mid(tag_position1 + 13, 18);
            qDebug() << "ip_h = " << ip_h;
            QString _origin = str.mid(tag_position2 + 16, 20);
            qDebug() << "_origin = " << _origin;
            QString lg_h = str.mid(tag_position3 + 13, 18);
            qDebug() << "lg_h = " << lg_h;
            QString to = str.mid(tag_position4 + 12, 220);
            qDebug() << "to = " << to;


     //Отправим новый запрос с параметрами и логином, паролем
            QString email = "0";

    QString authrequest = "https://login.vk.com/"
                          "?act=login"
            "&soft=1"
            "&utf8=1"
            "&_origin=" + _origin +
            "&lg_h=" + lg_h +
            "&ip_h=" + ip_h +
            "&to=" + to +
            "&email=" + login +
            "&pass=" + QUrl::toPercentEncoding(password);

    //QEventLoop loop;
    connect(na_manager,SIGNAL(finished(QNetworkReply*)),&loop,SLOT(quit()));

    QNetworkReply * reply2 = na_manager->get(
                    QNetworkRequest(
                        QUrl(authrequest)
                    )
                );

    loop.exec();

    str =reply2->header(QNetworkRequest::LocationHeader).toString();
    qDebug()<<"***РЕЗУЛЬТАТ 4 ЗАПРОСА HEADER"<< str << "\n\n" << authrequest;
    qDebug()<<"***РЕЗУЛЬТАТ 4 ЗАПРОСА BODY"<<reply2->readAll();
   // QString str2(reply2->readAll());
   //  qDebug() << "ответ от сервера2222 = " << str2;


    reply2 = na_manager->get(QNetworkRequest(QUrl(reply2->header(QNetworkRequest::LocationHeader).toString())));
    loop.exec();

    reply2 = na_manager->get(QNetworkRequest(QUrl(reply2->header(QNetworkRequest::LocationHeader).toString())));
    loop.exec();

    QString token, answer, id;
    answer = reply2->header(QNetworkRequest::LocationHeader).toString();
    token = answer.mid(answer.indexOf("access_token=") + 13, answer.indexOf("&expires_in") - answer.indexOf("access_token=") - 13);
    id = answer.mid(answer.indexOf("user_id=") + 8, answer.indexOf("&state=") - answer.indexOf("user_id=") - 8);

    qDebug() << "id\t" << id << "\n\n" << "token\t" << token;

    reply2 = na_manager->get(QNetworkRequest(QUrl("https://api.vk.com/method/friends.get?user_ids=" + id + "&fields=bdate&access_token=" + token + "&v=5.74")));
    loop.exec();
    answer = reply2->readAll();

    QString friends;

    friends = "https://api.vk.com/method/users.get?user_ids=";
    tag_position1 = 0;
    tag_position2 = 0;

    while (answer.indexOf("\"id\"", tag_position2) != -1) {
        tag_position1 = answer.indexOf("\"id\"", tag_position2) + 5;
        tag_position2 = answer.indexOf(",", tag_position1);
        friends += (answer.mid(tag_position1, tag_position2 - tag_position1)) + ",";
    }

    friends += "&fields=photo_100&access_token=" + token + "&v=5.74";

    reply2 = na_manager->get(QNetworkRequest(QUrl(friends)));
    loop.exec();

    answer = reply2->readAll();

    tag_position1 = 0;
    tag_position2 = 0;

    QString id_friend, name, photo;

    while (answer.indexOf("\"id\"", tag_position2) != -1) {
        tag_position1 = answer.indexOf("\"id\"", tag_position2) + 5;
        tag_position2 = answer.indexOf(",", tag_position1);
        id_friend = (answer.mid(tag_position1, tag_position2 - tag_position1));

        tag_position1 = answer.indexOf("\"first_name\"", tag_position2) + 14;
        tag_position2 = answer.indexOf("\"", tag_position1);
        name = answer.mid(tag_position1, tag_position2 - tag_position1);

        tag_position1 = answer.indexOf("\"last_name\"", tag_position2) + 13;
        tag_position2 = answer.indexOf("\"", tag_position1);
        name += " " + answer.mid(tag_position1, tag_position2 - tag_position1);


        tag_position1 = answer.indexOf("\"photo_100\"", tag_position2) + 13;
        tag_position2 = answer.indexOf("\"", tag_position1);
        photo = (answer.mid(tag_position1, tag_position2 - tag_position1).replace("\\", ""));


        friends_model.addFriend(FriendsObject(id_friend, name, photo));
    }
}
