#ifndef TCPCONTROLLER_H
#define TCPCONTROLLER_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>

class tcpcontroller : public QObject
{
    Q_OBJECT
public:
    explicit tcpcontroller(QObject *parent=nullptr);
     QTcpServer * serv;
     QTcpSocket * client_sock;
     QTcpSocket * server_sock;
     //QList<QTcpSocket*>socket_list; на случай необходимости поддержки нескольких соединений


signals:

public slots:
    void newConnection();
    void clientReady();
};

#endif // TCPCONTROLLER_H
