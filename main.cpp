#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "authcontroller.h"
#include <QQmlContext>
#include "cryptocontroller.h"
#include "db_controller.h"
#include "tcpcontroller.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    AuthController authController; //слот появляется здесь
    CryptoController cryptoController;
    cryptoController.encryptFile((unsigned char *) "12345678");
    cryptoController.decryptFile((unsigned char *) "123456798");

    db_controller database;
    database.sendQuieries();

    tcpcontroller tcp;

    QQmlApplicationEngine engine; //создается движок-контейнер отрисовки QML


    //ТИП СВЯЗЫВАНИЯ 3: Видимость С++ объекта в QML
    QQmlContext *context = engine.rootContext(); // создаем объект класса QQmlContext
    context->setContextProperty("modelFriends", &(authController.friends_model)); //Перемещаемая модель, которой присваиваем имя
    //Именно это функция добавляет в контекст (область видимости QML)

//lol
    engine.load(QUrl(QStringLiteral("qrc:/main.qml"))); //Стартовый QML файл

    if (engine.rootObjects().isEmpty())
        return -1;
    // сигнал потенциально может возникнуть здесь
    //ТИП СВЯЗЫВАНИЯ 1: Видимость QML объекта из С++
    //можно вызывать такой код только после загрузки движка и QML-файла
    QObject * appWindow = engine.rootObjects().first(); //ссылка на QML окно, обладателя сигнала
    //ТИП СВЯЗЫВАНИЯ 2: Связь QML-сигнала с С++-слотом
    //можно вызывать такой код только после загрузки движка,
    //QML-файла и объета обладателя слота
    QObject::connect(appWindow,
                     SIGNAL(sendAuth(QString,QString)),//чей и какой сигнал
                     &authController,
                     SLOT(Authentificate(QString,QString))); // к чему и какому слоту

    /*

    Тип связывания 4: Реакция QML на С++ Сигналы
    1. объявить кастомный сигнал в классе в разделе signals:
    2. Вызвать emit [имя сигнала]
    3. С помощью 3его способа сделать испускающий объект, видимый в QML
    4. С помощью Connections в QML запутить javaScript-код для сигнала onИмя_Сигнала
*/

    return app.exec();
}
/*
 *1) Создать пустой локальный репозиторий в директории с проектом командой
 * >git init
 *2)Нужно добавить отслеживаемые файлы командами git add [имя файла/папки]
 *3)Зарегестрировать свой юзернейм и почту
 *4)Зафикисровать изменения в истории (т.е. создать "Версию" или commit)
 *!!!Обязателен ввод комментария
 * >git commit -m"Текст комментария"
 *5)Как связать с сайтом?
 * >git remote add [Пвсевдоним] [ссылка.git]
 *  git remote add gitlab [ссылка.git]
 * 6)проверить можно командой
 * >git remote -v
 * 7)Последнюю прикрепленную версию отправить в репозиторий
 * >git push
 * или
 * >git push [псевдоним] [название ветки] пример:git push gitlab master
 * 8)Копировать с удаленного репозитория командой
 * >git clone ссылка.git
 *9) Если текущий локальный репозиторий отстал от удаленного, сделать его снова актуальной командой
 * >git pull
 */
