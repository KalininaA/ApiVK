#ifndef CRYPTOCONTROLLER_H
#define CRYPTOCONTROLLER_H

#include <QObject>

class CryptoController : public QObject
{
    Q_OBJECT
public:
    explicit CryptoController(QObject *parent = nullptr);

signals:

public slots:
    void encryptFile (unsigned char * key);
    void decryptFile (unsigned char * key);
};

#endif // CRYPTOCONTROLLER_H
